Faker::Commerce.department(10, true).split(/[\,&]/).each do |department|
  Detail.create!(
    department:  department.strip,
    color:       Faker::Commerce.color,
    material:    Faker::Commerce.material
  )
end

50.times do
  product = Product.create!(
    name:       Faker::Commerce.product_name,
    price:      Faker::Commerce.price,
    detail:     Detail.all.sample
  )
  Promotion.create!(
    code:       Faker::Commerce.promotion_code,
    active:     true,
    product:    product
  )
end

