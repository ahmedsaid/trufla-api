class CreateDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :details do |t|
      t.string :department
      t.string :color
      t.string :material

      t.timestamps
    end
  end
end
