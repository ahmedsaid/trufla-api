if @products
  json.state :success
  json.products @products do |product|
    json.(product, :id, :name, :price)
    json.(product.detail, :department, :material, :color)
    json.(product, :created_at, :updated_at)
    json.promotions product.promotions do |promotion|
      json.(promotion, :code, :active, :id)
    end
  end
else
  json.state :error
  json.message 'Error'
end
