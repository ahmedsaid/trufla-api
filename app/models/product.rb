class Product < ApplicationRecord
  extend PagedScope  # cutom pagination 
  has_many :promotions
  belongs_to :detail
end
