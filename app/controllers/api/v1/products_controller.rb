module Api::V1
  class ProductsController < ApplicationController
    def index
      @products = Product.pagination(page: params[:page])
        .includes(:promotions,:detail) 
    end
  end
end
